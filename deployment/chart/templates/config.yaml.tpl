kind: ConfigMap
apiVersion: v1
metadata:
  name: volumes-config
data:
  volumes.json: {{ .Values.volumes | toJson | toYaml }}
